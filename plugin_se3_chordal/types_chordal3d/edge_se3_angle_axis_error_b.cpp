#include "edge_se3_angle_axis_error_b.h"
#include "chordal_mappings.h"

#ifdef G2O_HAVE_OPENGL
#include "g2o/stuff/opengl_wrapper.h"
#include "g2o/stuff/opengl_primitives.h"
#endif

namespace g2o {

  EdgeSE3AngleAxisErrorB::EdgeSE3AngleAxisErrorB():
          BaseBinaryEdge<6, Isometry3, VertexSE3, VertexSE3>() {
    _inverse_measurement.setIdentity();
    _information.setIdentity();
  }


  bool EdgeSE3AngleAxisErrorB::read(std::istream& is_) {
    //ia measurement
    Vector7 meas = Vector7::Zero();
    for (uint16_t i = 0; i < 7; i++)
      is_ >> meas[i];

    // normalize the quaternion to recover numerical precision lost by storing as human readable text
    Vector4::MapType(meas.data()+3).normalize();
    setMeasurement(internal::fromVectorQT(meas));
    if (is_.bad()) {
      return false;
    }

    //ia information matrix
    for ( int i=0; i<information().rows() && is_.good(); i++)
      for (int j=i; j<information().cols() && is_.good(); j++){
        is_ >> information()(i,j);
        if (i!=j)
          information()(j,i)=information()(i,j);
      }
    if (is_.bad()) {
      //  we overwrite the information matrix with the Identity
      information().setIdentity();
    }
    return true;
  }

  bool EdgeSE3AngleAxisErrorB::write(std::ostream& os_) const {
    Vector7 meas=internal::toVectorQT(_measurement);
    for (uint16_t i = 0; i < 7; i++)
      os_ << meas[i] << " ";
    for (uint16_t i = 0; i < information().rows(); i++)
      for (uint16_t j = i; j < information().cols(); j++) {
        os_ <<  information()(i,j) << " ";
      }
    return os_.good();
  }

  bool EdgeSE3AngleAxisErrorB::setMeasurementFromState() {
    VertexSE3* from = static_cast<VertexSE3*>(_vertices[0]);
    VertexSE3* to   = static_cast<VertexSE3*>(_vertices[1]);
    Isometry3 delta = from->estimate().inverse() * to->estimate();
    setMeasurement(delta);
    return true;
  }

  void EdgeSE3AngleAxisErrorB::initialEstimate(const OptimizableGraph::VertexSet& from_,
                                               OptimizableGraph::Vertex* /*to_*/) {
    VertexSE3* from = static_cast<VertexSE3*>(_vertices[0]);
    VertexSE3* to = static_cast<VertexSE3*>(_vertices[1]);
    if (from_.count(from) > 0) {
      to->setEstimate(from->estimate() * _measurement);
    } else {
      from->setEstimate(to->estimate() * _inverse_measurement);
    }
  }

  void EdgeSE3AngleAxisErrorB::computeError() {
    VertexSE3* from = dynamic_cast<VertexSE3*>(_vertices[0]);
    VertexSE3* to   = dynamic_cast<VertexSE3*>(_vertices[1]);
    Isometry3 delta = _inverse_measurement * from->estimate().inverse() * to->estimate();
    _error=internal::toVectorSAA(delta);
  }



#ifdef G2O_HAVE_OPENGL
  EdgeSE3AngleAxisErrorBDrawAction::EdgeSE3AngleAxisErrorBDrawAction() :
      DrawAction(typeid(EdgeSE3AngleAxisErrorB).name()) {
    //ia parent ctor
  }

  HyperGraphElementAction* EdgeSE3AngleAxisErrorBDrawAction::operator()(HyperGraph::HyperGraphElement* element,
                                                                        HyperGraphElementAction::Parameters* params) {
    if (typeid(*element).name()!=_typeName)
      return 0;
    refreshPropertyPtrs(params);
    if (! _previousParams)
      return this;

    if (_show && !_show->value())
      return this;

    EdgeSE3AngleAxisErrorB* e =  static_cast<EdgeSE3AngleAxisErrorB*>(element);
    VertexSE3* from = static_cast<VertexSE3*>(e->vertices()[0]);
    VertexSE3* to   = static_cast<VertexSE3*>(e->vertices()[1]);
    if (! from || ! to)
      return this;
    glColor3f(POSE_EDGE_COLOR);
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    glBegin(GL_LINES);
    glVertex3f((float)from->estimate().translation().x(),
               (float)from->estimate().translation().y(),
               (float)from->estimate().translation().z());
    glVertex3f((float)to->estimate().translation().x(),
               (float)to->estimate().translation().y(),
               (float)to->estimate().translation().z());
    glEnd();
    glPopAttrib();
    return this;
  }
#endif

} /* namespace g2o */
