#pragma once
//ia include fancy colors
#include "g2o/stuff/color_macros.h"
//ia types
#include "g2o/types/slam3d/isometry3d_mappings.h"
#include "g2o/types/slam3d/types_slam3d.h"
#include "types_chordal3d.h"

#include "g2o/stuff/unscented.h"
namespace g2o {

  class OmegaConverterSE3 {
  public:
    //! @brief typedefs
    using IntPair = std::pair<int, int>;
    using SigmaPoint6 = SigmaPoint<Vector6>;
    using SigmaPoint12 = SigmaPoint<Vector12>;
    using SigmaPoint6Vector = std::vector<SigmaPoint6,  Eigen::aligned_allocator<SigmaPoint6> >;
    using SigmaPoint12Vector = std::vector<SigmaPoint12, Eigen::aligned_allocator<SigmaPoint12 > >;

    OmegaConverterSE3() {}
    virtual ~OmegaConverterSE3() {}

    //! @brief convert omega from quaternion to euler
    static Matrix6 convertOmegaQuaternion2Euler(const Vector6& mean_quaternion_,
                                                const Matrix6& omega_quaternion_);

    //! @brief convert omega from euler to quaternion
    static Matrix6 convertOmegaEuler2Quaternion(const Vector6& mean_euler_,
                                                const Matrix6& omega_euler_);

    //! @brief convert omega from quaternion to chordal
    static Matrix12 convertOmegaQuaternion2Chordal(const Vector6& mean_quaternion_,
                                                   const Matrix6& omega_quaternion_,
                                                   const float& thresh_);

    //! @brief convert omega from chordal to quaternion
    static Matrix6 convertOmegaChordal2Quaternion(const Vector12& mean_chordal_,
                                                  const Matrix12& omega_chordal_);

    //! @brief convert omega from quaternion to scaled angle axis
    static Matrix6 convertOmegaQuaternion2AngleAxis(const Vector6& mean_quaternion_,
                                                    const Matrix6& omega_quaternion_);

    //! @brief convert omega from scaled angle axis to quaternion
    static Matrix6 convertOmegaAngleAxis2Quaternion(const Vector6& mean_angle_axis_,
                                                    const Matrix6& omega_angle_axis_);

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} /* namespace g2o */

