#include "types_chordal3d.h"
#include "g2o/core/factory.h"
#include "g2o/stuff/macros.h"

#include <iostream>

namespace g2o {
  //ia group
  G2O_REGISTER_TYPE_GROUP(chordal3d);

  //ia types
  G2O_REGISTER_TYPE(VERTEX_SE3:EULERPERT, VertexSE3EulerPert);
  G2O_REGISTER_TYPE(VERTEX_SE3:ANAXPERT, VertexSE3AngleAxisPert);
  G2O_REGISTER_TYPE(EDGE_SE3:QUAT-A, EdgeSE3QuaternionErrorA);
  G2O_REGISTER_TYPE(EDGE_SE3:CHORD-A, EdgeSE3ChordalErrorA);
  G2O_REGISTER_TYPE(EDGE_SE3:CHORD-B, EdgeSE3ChordalErrorB);
  G2O_REGISTER_TYPE(EDGE_SE3:EULER-A, EdgeSE3EulerErrorA);
  G2O_REGISTER_TYPE(EDGE_SE3:EULER-B, EdgeSE3EulerErrorB);
  G2O_REGISTER_TYPE(EDGE_SE3:ANAX-A, EdgeSE3AngleAxisErrorA);
  G2O_REGISTER_TYPE(EDGE_SE3:ANAX-B, EdgeSE3AngleAxisErrorB);
  G2O_REGISTER_TYPE(EDGE_SE3:ANAX-C, EdgeSE3AngleAxisErrorC);


  //ia actions
#ifdef G2O_HAVE_OPENGL
  G2O_REGISTER_ACTION(VertexSE3EulerPertDrawAction);
  G2O_REGISTER_ACTION(VertexSE3AngleAxisPertDrawAction);
  G2O_REGISTER_ACTION(EdgeSE3QuaternionErrorADrawAction);
  G2O_REGISTER_ACTION(EdgeSE3ChordalErrorADrawAction);
  G2O_REGISTER_ACTION(EdgeSE3ChordalErrorBDrawAction);
  G2O_REGISTER_ACTION(EdgeSE3EulerErrorADrawAction);
  G2O_REGISTER_ACTION(EdgeSE3EulerErrorBDrawAction);
  G2O_REGISTER_ACTION(EdgeSE3AngleAxisErrorADrawAction);
  G2O_REGISTER_ACTION(EdgeSE3AngleAxisErrorBDrawAction);
  G2O_REGISTER_ACTION(EdgeSE3AngleAxisErrorCDrawAction);
#endif

} //ia end namespace g2o
