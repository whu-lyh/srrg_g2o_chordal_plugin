#include "chordal_mappings.h"
#include "g2o/types/slam3d/isometry3d_gradients.h"
#include "g2o/types/slam3d/isometry3d_mappings.h"
#include <iostream>

namespace g2o {
  namespace internal {

    Vector12 toFlatten(const Isometry3& t) {
      Vector12 v = Vector12::Zero();
      v.block<3,1>(0,0) = t.matrix().block<3,1>(0,0);
      v.block<3,1>(3,0) = t.matrix().block<3,1>(0,1);
      v.block<3,1>(6,0) = t.matrix().block<3,1>(0,2);
      v.block<3,1>(9,0) = t.matrix().block<3,1>(0,3);
      return v;
    }
    
    Vector12 flattenByRow(const Isometry3& t) {
      Vector12 v = Vector12::Zero();
      v.block<3,1>(0,0) = t.matrix().block<1,3>(0,0).transpose();
      v.block<3,1>(3,0) = t.matrix().block<1,3>(1,0).transpose();
      v.block<3,1>(6,0) = t.matrix().block<1,3>(2,0).transpose();
      v.block<3,1>(9,0) = t.matrix().block<3,1>(0,3);
      return v;
    }

      
    Isometry3 fromFlatten(const Vector12& v,
                          const bool reconditionate_rotation) {
      Isometry3 t = Isometry3::Identity();
      t.matrix().block<3,1>(0,0) = v.block<3,1>(0,0);
      t.matrix().block<3,1>(0,1) = v.block<3,1>(3,0);
      t.matrix().block<3,1>(0,2) = v.block<3,1>(6,0);
      t.matrix().block<3,1>(0,3) = v.block<3,1>(9,0);

      if (reconditionate_rotation) {
        const Matrix3& R = t.linear();
        Eigen::JacobiSVD<Matrix3> svd(R, Eigen::ComputeThinU | Eigen::ComputeThinV);
        Matrix3 R_enforced = svd.matrixU() * svd.matrixV().transpose();
        t.linear() = R_enforced;
      }

      return t;
    }
    

    Isometry3 fromFlattenByRow(const Vector12& v,
                               const bool reconditionate_rotation) {
      Isometry3 T = Isometry3::Identity();
      T.matrix().block<1,3>(0,0) = v.block<3,1>(0,0).transpose();
      T.matrix().block<1,3>(1,0) = v.block<3,1>(3,0).transpose();
      T.matrix().block<1,3>(2,0) = v.block<3,1>(6,0).transpose();
      T.matrix().block<3,1>(0,3) = v.block<3,1>(9,0);

      //ia check the determinant
      if (reconditionate_rotation) {
        const Matrix3& R = T.linear();
        Eigen::JacobiSVD<Matrix3> svd(R, Eigen::ComputeThinU | Eigen::ComputeThinV);
        Matrix3 R_enforced = svd.matrixU() * svd.matrixV().transpose();
        T.linear() = R_enforced;
      }

      return T;
    }



    Matrix12 mprod(const Isometry3& T) {
      Matrix12 M = Matrix12::Identity();
      const Matrix3 Rt = T.linear().transpose();
      const Vector3 tt = T.translation().transpose();

      M.block<3,3>(0,0) = Rt;
      M.block<3,3>(3,3) = Rt;
      M.block<3,3>(6,6) = Rt;

      M.block<1,3>(9,0) = tt;
      M.block<1,3>(10,3) = tt;
      M.block<1,3>(11,6) = tt;
      return M;
    }


    Vector6 toVectorSAA(const Isometry3& T) {
      Vector6 v = Vector6::Zero();
      //ia translation - easy
      v.head<3>() = T.translation();
      //ia rotation
      AngleAxis aa(T.linear());
      number_t angle = aa.angle();
      Vector3 axis = aa.axis();
      angle = std::atan2(sin(angle), cos(angle));

      v.tail<3>() = axis*angle;

//      std::cerr << "toVectorSAA|angle = " << angle << std::endl;
//      std::cerr << "toVectorSAA|axis  = " << axis.transpose() << std::endl;

      return v;
    }

    Isometry3 fromVectorSAA(const Vector6& v) {
      Isometry3 T = Isometry3::Identity();
      //ia translation - easy
      T.translation() = v.head<3>();
      //ia rotation
      const Vector3 saa = v.tail<3>();
      float angle = saa.norm();
      Vector3 axis = Vector3(1,0,0);

      if (std::fabs(angle)<1e-6) {
        return T;
      }

      axis = saa.normalized();
      angle = std::atan2(sin(angle), cos(angle));

//      std::cerr << "fromVectorSAA|angle = " << angle << std::endl;
//      std::cerr << "fromVectorSAA|axis  = " << axis.transpose() << std::endl;

      AngleAxis aa(angle, axis);
      T.linear() = aa.toRotationMatrix();
      return T;
    }


    Vector7 toVectorAA(const Isometry3& T) {
      Vector7 v = Vector7::Zero();
      //ia translation - easy
      v.head<3>() = T.translation();
      //ia rotation
      AngleAxis aa(T.linear());
      v.block<3,1>(3,0) = aa.axis();
      v[6] = aa.angle();
      return v;
    }

    Isometry3 fromVectorAA(const Vector7& v) {
      Isometry3 T = Isometry3::Identity();
      //ia translation - easy
      T.translation() = v.head<3>();
      //ia rotation
      AngleAxis aa(v[6], v.block<3,1>(3,0));
      T.linear() = aa.toRotationMatrix();
      return T;
    }


  } //ia end namespace internal
} //ia end namespace g2o
