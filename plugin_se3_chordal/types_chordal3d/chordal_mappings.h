#pragma once

#include "g2o/config.h"
#include "g2o/types/slam3d/g2o_types_slam3d_api.h"
#include <Eigen/Core>
#include <Eigen/Geometry>

namespace g2o {

  //! @brief eigen types
  using Vector9  = VectorN<9>;
  using Vector12 = VectorN<12>;
  using Matrix6  = Eigen::Matrix<number_t,6,6,Eigen::ColMajor>;
  using Matrix9  = Eigen::Matrix<number_t,9,9,Eigen::ColMajor>;
  using Matrix12 = Eigen::Matrix<number_t,12,12,Eigen::ColMajor>;

  namespace internal {

    //! @brief converts an Isometry3 into a 12 vector [rx ry rz t]
    G2O_TYPES_SLAM3D_API Vector12 toFlatten(const Isometry3& t);
    G2O_TYPES_SLAM3D_API Vector12 flattenByRow(const Isometry3& t);

    //! @brief converts a 12 vector [rx ry rz t] into an Isometry3;
    //! @param reconditionate_rotation: if true reconditionate the
    //!        rotation matrix R.
    G2O_TYPES_SLAM3D_API Isometry3 fromFlatten(const Vector12& v, const bool reconditionate_rotation);
    G2O_TYPES_SLAM3D_API Isometry3 fromFlattenByRow(const Vector12& v, const bool reconditionate_rotation);

    //! @brief magical thing for isometry multiplication
    G2O_TYPES_SLAM3D_API Matrix12 mprod(const Isometry3& T);

    //! @brief convert an Isometry3 into a 6 dimensional vector [t scaled_axis_angle]
    //!        the scaled_axis_angle is scaled by the angle so it is a 3D vector
    G2O_TYPES_SLAM3D_API Vector6 toVectorSAA(const Isometry3& T);

    //! @brief convert an Isometry3 into a 7 dimensional vector [t axis_angle]
    //!        the axis_angle is a 4D vector [axis | angle]
    G2O_TYPES_SLAM3D_API Vector7 toVectorAA(const Isometry3& T);

    //! @brief convert a 6 dimensional [t scaled_axis_angle] vector into an Isometry3
    //!        the scaled_axis_angle is scaled by the angle so it is a 3D vector
    G2O_TYPES_SLAM3D_API Isometry3 fromVectorSAA(const Vector6& v);

    //! @brief convert a 7 dimensional vector [t axis_angle] into an Isometry3
    //!        the axis_angle is a 4D vector [axis | angle]
    G2O_TYPES_SLAM3D_API Isometry3 fromVectorAA(const Vector7& v);

  } //ia end namespace internal
  
} //ia end namespace g2o
