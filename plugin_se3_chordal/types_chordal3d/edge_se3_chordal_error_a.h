#pragma once

#include "g2o/core/base_binary_edge.h"
#include "g2o/types/slam3d/g2o_types_slam3d_api.h"

#include "vertex_se3_euler_pert.h"

namespace g2o {

  /**
   * \brief Edge between two 3D pose vertices (with euler angle based increment)
   *
   * The transformation between the two vertices is given as an Isometry3.
   * If z denotes the measurement, then the error function is given as follows:
   * flat(z^-1) - flat(x_i^-1 * x_j)
   */
  class G2O_TYPES_SLAM3D_API EdgeSE3ChordalErrorA
    : public BaseBinaryEdge<12,
                            Isometry3,
                            VertexSE3EulerPert,
                            VertexSE3EulerPert> {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    //! @brief ctor
    EdgeSE3ChordalErrorA();

    //! @brief read/write in a stream
    virtual bool read(std::istream& is);
    virtual bool write(std::ostream& os) const;

    //! @brief computes the error according to the approximation
    //!        of the chordal distance between the two quantities
    void computeError();

    //! @brief computes the right jacobians
    void linearizeOplus();

    void setMeasurement(const Isometry3& meas) {
      _measurement = meas;
    }

    virtual bool setMeasurementFromState();

    virtual number_t
    initialEstimatePossible(const OptimizableGraph::VertexSet& /*from*/,
                            OptimizableGraph::Vertex* /*to*/) {
      return 1.;
    }

    virtual void initialEstimate(const OptimizableGraph::VertexSet& /*from_*/,
                                 OptimizableGraph::Vertex* /*to*/);
  };

#ifdef G2O_HAVE_OPENGL
  //! @brief visualization
  class G2O_TYPES_SLAM3D_API EdgeSE3ChordalErrorADrawAction
    : public DrawAction {
  public:
    EdgeSE3ChordalErrorADrawAction();
    virtual HyperGraphElementAction*
    operator()(HyperGraph::HyperGraphElement* element,
               HyperGraphElementAction::Parameters* params);
  };
#endif

} // namespace g2o
