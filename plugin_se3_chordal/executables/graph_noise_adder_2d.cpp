#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cmath>

//ia include types
#include "g2o/types/slam2d/se2.h"
#include "g2o/types/slam2d/types_slam2d.h"
#include "g2o/types/slam3d/types_slam3d.h"
#include "g2o/types/slam3d/isometry3d_mappings.h"

//ia include g2o core stuff
#include "g2o/stuff/sampler.h"
#include "g2o/stuff/command_args.h"
#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/factory.h"

//ia dynamic loaders
#include "g2o/apps/g2o_cli/dl_wrapper.h"
#include "g2o/apps/g2o_cli/g2o_common.h"
#include "g2o/apps/g2o_cli/output_helper.h"

using namespace g2o;
const std::string exe_name("graph_noise_adder_2d");
#define LOG std::cerr << exe_name + "|"

// ia checks whether file exists
bool checkFile(const std::string& name_) {
  struct stat buffer;
  return (stat(name_.c_str(), &buffer) == 0);
}

//ia some usings
using Matrix1 = g2o::MatrixN<1>;
using Vector1 = g2o::VectorN<1>;
using GaussianSampler3 = g2o::GaussianSampler<Vector3, Matrix3>;
using GaussianSampler2 = g2o::GaussianSampler<Vector2, Matrix2>;
using GaussianSampler1 = g2o::GaussianSampler<Vector1, Matrix1>;

//ia add noise to pose
void addNoiseSE2(g2o::HyperGraph::Edge* edge_,
                 GaussianSampler2& sampler_t_,
                 GaussianSampler1& sampler_r_,
                 const Matrix3& omega_);

//ia add noise to points
void addNoiseR2(g2o::HyperGraph::Edge* edge_,
                GaussianSampler2& sampler_xy_,
                const Matrix2& omega_);

int main(int argc, char** argv) {
  LOG << "this executable adds AWGN to 2D pose graph or a pose-landmark graph [for now]\n";
  LOG << "INPUT GRAPH SHOULD BE AT THE OPTIMUM\n\n";

  //ia registering all the types from the libraries
  g2o::DlWrapper dlTypesWrapper;
  loadStandardTypes(dlTypesWrapper, argc, argv);
  g2o::DlWrapper dlSolverWrapper;
  loadStandardSolver(dlSolverWrapper, argc, argv);

  //ia command line args
  bool param_use_random_seed = false;
  number_t param_noise_pose_r = -1.f;
  std::vector<number_t> param_noise_pose_t;
  std::vector<number_t> param_noise_point;
  std::string param_output_file = "";
  std::string param_input_file = "";

  //ia parse command line
  g2o::CommandArgs arg;
  arg.param("o",
            param_output_file,
            "",
            "output graph (noisyfied)");
  arg.param("pointSigma",
            param_noise_point,
            std::vector<number_t>(),
            "semicolumn-spaced Sigma for the points - default: \"0.01;0.01\"");
  arg.param("poseSigma_t",
            param_noise_pose_t,
            std::vector<number_t>(),
            "semicolumn-spaced Sigma for the pose noise, trans - default: \"0.01;0.01\"");
  arg.param("poseSigma_r",
            param_noise_pose_r,
            0.005,
            "one dimensional Sigma for the pose noise, rot - default: 0.005");
  arg.param("randomSeed",
            param_use_random_seed,
            false,
            "use a random seed to sample noise");
  arg.paramLeftOver("inputGraph",
                    param_input_file,
                    "",
                    "input graph to be processed");
  arg.parseArgs(argc, argv);

  //ia check if input file exists
  if (!checkFile(param_input_file)) {
    throw std::runtime_error(exe_name + "|ERROR, input file [ " +
                             param_input_file + " ] does not exists");
  }


  //ia use default noise values if not set
  if (param_noise_pose_t.size() == 0) {
    LOG << "WARNING, using default Sigma for the pose (translation)\n";
    param_noise_pose_t.push_back(0.01);
    param_noise_pose_t.push_back(0.01);
  }

  if (param_noise_pose_r < 0 || param_noise_pose_r == 0.005) {
    LOG << "WARNING, using default Sigma for the pose (rotation)\n";
    param_noise_pose_r = 0.005f;
  }

  if (param_noise_point.size() == 0) {
    LOG << "WARNING, using default Sigma for the points\n";
    param_noise_point.push_back(0.01);
    param_noise_point.push_back(0.01);
  }

  //ia compute stddevs
  Matrix1 sigma_pose_r = Matrix1::Identity();
  sigma_pose_r(0,0) = std::pow(param_noise_pose_r, 2);
  
  Matrix2 sigma_pose_t = Matrix2::Identity();
  Matrix2 sigma_point  = Matrix2::Identity();

  for (size_t i = 0; i < (size_t)2; ++i) {
    sigma_pose_t(i,i) = std::pow(param_noise_pose_t[i], 2);
    sigma_point(i,i)  = std::pow(param_noise_point[i], 2);
  }
  
  //ia generate full omega for pose-pose and pose-points measurements according to noise statistics
  Matrix3 omega_pose_meas = Matrix3::Zero();
  omega_pose_meas.block<2,2>(0,0) = sigma_pose_t.inverse();
  omega_pose_meas.block<1,1>(2,2) = sigma_pose_r.inverse();

  Matrix2 omega_point_meas = Matrix2::Zero();
  omega_point_meas = sigma_point.inverse();

  //ia log something
  LOG << "noise sigma pose (translational)\n" << sigma_pose_t << std::endl;
  LOG << "noise sigma pose (rotational)\n" << sigma_pose_r << std::endl;
  LOG << "noise sigma point\n" << sigma_point << std::endl;
  LOG << "omega pose-pose\n" << omega_pose_meas << std::endl;
  LOG << "omega pose-point\n" << omega_point_meas << std::endl;

  
  //ia load the input graph
  LOG << "loading file [ " << param_input_file << " ]\n";
  g2o::SparseOptimizer optimizer;
  std::ifstream ifs(param_input_file.c_str());
  optimizer.load(ifs);

  const HyperGraph::VertexIDMap& src_vertices = optimizer.vertices();
  const HyperGraph::EdgeSet& src_edges = optimizer.edges();

  const size_t num_v = src_vertices.size();
  const size_t num_e = src_edges.size();
  LOG << "input file has [ "
      << num_v << " ] vertices -- [ "
      << num_e << " ] edges\n";

  //ia setup noise generators
  GaussianSampler2 sampler_pose_t;
  GaussianSampler1 sampler_pose_r;
  GaussianSampler2 sampler_point;
  sampler_pose_t.setDistribution(sigma_pose_t);
  sampler_pose_r.setDistribution(sigma_pose_r);
  sampler_point.setDistribution(sigma_point);

  //ia setup random seeds for the samplers
  if (param_use_random_seed) {
    LOG << "using random seeds to sample noise\n";

    std::vector<int> seeds(3);
    std::random_device randomizer;
    std::seed_seq seed_sequence{randomizer(), randomizer(), randomizer(), randomizer(), randomizer()};
    seed_sequence.generate(seeds.begin(), seeds.end());
    assert(seeds.size() == 3);

    for (size_t i = 0; i < seeds.size(); ++i) {
      LOG << "seed [ " << i << " ] = " << seeds[i] << std::endl;
    }

    if (!sampler_pose_t.seed(seeds[0])) {
      throw std::runtime_error(exe_name+"|ERROR, cannot set random seed");
    }
    if (!sampler_pose_r.seed(seeds[1])) {
      throw std::runtime_error(exe_name+"|ERROR, cannot set random seed");
    }
    if (!sampler_point.seed(seeds[2])) {
      throw std::runtime_error(exe_name+"|ERROR, cannot set random seed");
    }
  }

  //ia now we have to generate new noisy measurements
  size_t cnt = 0, num_se2_edges = 0, num_r2_edges = 0, num_discarded_edges = 0;
  for (HyperGraph::Edge* eptr : src_edges) {
    //ia process each edge differently TODO super shitty
    const std::string edge_tag = Factory::instance()->tag(eptr);
    if (edge_tag == "EDGE_SE2") {
      //ia adding noise to pose pose edges
      addNoiseSE2(eptr, sampler_pose_t, sampler_pose_r, omega_pose_meas);
      ++num_se2_edges;
    } else if (edge_tag == "EDGE_SE2_XY") {
      //ia adding noise to pose point edges
      addNoiseR2(eptr, sampler_point, omega_point_meas);
      ++num_r2_edges;
    } else {
      ++num_discarded_edges;
    }

    float percentage = std::ceil((float)cnt++ / (float)num_e * 100.f);
    if ((int)percentage % 5 == 0) {
      std::cerr << "\r" << exe_name << "|processed " << percentage << "%" << std::flush;
    }
  }

  //ia log
  std::cerr << std::endl;
  LOG << "processed [ "
      << num_se2_edges << "/" << cnt << " ] se2 edges -- [ "
      << num_r2_edges << "/" << cnt << " ] r2 edges ---- [ "
      << num_discarded_edges << "/" << cnt << " ] discarded edges\n";

  //ia save and checkout
  if (param_output_file.empty()) {
    LOG << "WARNING, no output file specified, graph will be lost\n";
  } else {
    LOG << "saving output in [ " << param_output_file << " ]\n";
    optimizer.save(param_output_file.c_str());
  }

  LOG << "done!\n";
  return 0;
}

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
void addNoiseSE2(g2o::HyperGraph::Edge* edge_,
                 GaussianSampler2& sampler_t_,
                 GaussianSampler1& sampler_r_,
                 const Matrix3& omega_) {
  EdgeSE2* e = dynamic_cast<EdgeSE2*>(edge_);
  if (!e) {
    throw std::runtime_error(exe_name+"|ERROR, invalid cast to EdgeSE2");
  }

  //ia estimate GT edge
  if (!e->setMeasurementFromState()) {
    throw std::runtime_error(exe_name+"|ERROR, cannot set measurement from state");
  }
  
  const g2o::SE2 Z_gt = e->measurement();
  const Vector2 trans_gt = Z_gt.translation();
  const number_t angle_gt = Z_gt.rotation().angle();
  
  //ia generate noisy measurement
  const Vector2 trans_noisy = trans_gt + sampler_t_.generateSample();
  const number_t angle_noisy = normalize_theta(angle_gt + sampler_r_.generateSample()(0,0));

  SE2 noisy_meas;
  noisy_meas.setTranslation(trans_noisy);
  noisy_meas.setRotation(g2o::Rotation2D(angle_noisy));

  e->setMeasurement(noisy_meas);
  e->setInformation(omega_);
}


// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
void addNoiseR2(g2o::HyperGraph::Edge* edge_,
                GaussianSampler2& sampler_xyz_,
                const Matrix2& omega_) {
  EdgeSE2PointXY* e = dynamic_cast<EdgeSE2PointXY*>(edge_);
  if (!e) {
    throw std::runtime_error(exe_name+"|ERROR, invalid cast to EdgeSE2PointXY");
  }

  //ia estimate GT edge from current state (supposing that it is at the optimum)
  if (!e->setMeasurementFromState()) {
    throw std::runtime_error(exe_name+"|ERROR, cannot set measurement from state");
  }

  //ia add noise
  const Vector2 noise_xyz = sampler_xyz_.generateSample();
  const Vector2 noisy_meas = e->measurement() + noise_xyz;

  e->setMeasurement(noisy_meas);
  e->setInformation(omega_);  
}
