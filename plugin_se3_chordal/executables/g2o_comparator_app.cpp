#include <algorithm>
#include <cassert>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <signal.h>
#include <sstream>
#include <string>

#include "g2o/apps/g2o_cli/dl_wrapper.h"
#include "g2o/apps/g2o_cli/g2o_common.h"
#include "g2o/apps/g2o_cli/output_helper.h"

#include "g2o/config.h"
#include "g2o/core/batch_stats.h"
#include "g2o/core/estimate_propagator.h"
#include "g2o/core/factory.h"
#include "g2o/core/hyper_dijkstra.h"
#include "g2o/core/hyper_graph_action.h"
#include "g2o/core/optimization_algorithm_factory.h"
#include "g2o/core/sparse_optimizer.h"

#include "g2o/core/optimization_algorithm.h"
#include "g2o/core/robust_kernel.h"
#include "g2o/core/robust_kernel_factory.h"
#include "g2o/core/sparse_optimizer_terminate_action.h"

#include "g2o/stuff/color_macros.h"
#include "g2o/stuff/command_args.h"
#include "g2o/stuff/filesys_tools.h"
#include "g2o/stuff/macros.h"
#include "g2o/stuff/string_tools.h"
#include "g2o/stuff/timeutil.h"

#include "g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h"
#include "g2o/plugin_se3_chordal/types_chordal_initialization3d/types_chordal_initialization3d.h"
#include "g2o/types/slam3d/types_slam3d.h"

// ia include unscented
#include "g2o/stuff/unscented.h"

// ia include chordal initializer
#include "g2o/plugin_se3_chordal/types_chordal_initialization3d/chordal_initializer_se3.h"
#include "optimization_stats.h"

using namespace std;
using namespace g2o;

// @brief omega remapper
// ia useful typedefs
typedef std::pair<int, int> IntPair;
typedef SigmaPoint<Vector6> SigmaPoint6;
typedef SigmaPoint<Vector12> SigmaPoint12;

typedef std::vector<SigmaPoint<Vector6>,
                    Eigen::aligned_allocator<SigmaPoint<Vector6>>>
  SigmaPoint6Vector;
typedef std::vector<SigmaPoint<Vector12>,
                    Eigen::aligned_allocator<SigmaPoint<Vector12>>>
  SigmaPoint12Vector;
Matrix12 _remapInformationMatrix(const Vector6& src_mean_,
                                 const Matrix6& src_omega_,
                                 const double& thresh_);

Matrix12 _reconditionateSigma(const Matrix12& src_sigma_,
                              const double& threshold_);

//! @brief this is called in the *preiteration* so
//!        this lives one iteration in the past :)
//!        If you make the stats from this guy, than
//!        you have to give an extra iteration (101 iterations)
struct ComparatorAction : public HyperGraphAction {
  //! @brief ptr to the geodesic opt
  SparseOptimizer* geo_opt_ptr = 0;
  //! @brief ptr to the chordal opt - also for stats
  SparseOptimizer* chord_opt_ptr = 0;
  //! @brief ptr to the stream where I will write the stats
  OptimizationComparisonStatsVector stats_vector;
  //! @brief robust kernel flag
  bool kernel_flag = false;
  //! @brief shits out things
  const bool verbose_flag;
  //! @brief evaluate linearity of the cost function
  const bool evaluate_error_function_linearity;
  //! @brief delta chi2 between two iterations
  number_t prev_chi2 = 0.0;

  //! @breif ctor
  ComparatorAction(SparseOptimizer* geodesic_opt_,
                   SparseOptimizer* chordal_opt_,
                   bool kernel_flag_,
                   bool verbose_flag_,
                   bool linearity_flag_ = true) :
    geo_opt_ptr(geodesic_opt_),
    chord_opt_ptr(chordal_opt_),
    kernel_flag(kernel_flag_),
    verbose_flag(verbose_flag_),
    evaluate_error_function_linearity(linearity_flag_) {
    if (!geo_opt_ptr || !chord_opt_ptr)
      throw std::runtime_error(
        "ComparatorAction::ComparatorAction|you set a null sparse optimizer");
    stats_vector.clear();
  }

  //! @brief all the shit is here :)
  HyperGraphAction* operator()(const HyperGraph* graph,
                               Parameters* parameters) override {
    int current_iteration = -10;
    // ia if everything is still null than do nothing
    if (!graph)
      return 0;

    // ia if there is an iteration number than print it in the stats
    ParametersIteration* param_it =
      dynamic_cast<ParametersIteration*>(parameters);
    if (param_it) {
      if (param_it->iteration < 0)
        return 0;

      current_iteration = param_it->iteration - 1;
    }

    // ia copy the damn vertices in the geodesic graph and compute the chi2
    HyperGraph::VertexIDMap::iterator v_it  = chord_opt_ptr->vertices().begin();
    HyperGraph::VertexIDMap::iterator v_end = chord_opt_ptr->vertices().end();
    while (v_it != v_end) {
      // ia try to gather vertex type
      Isometry3 transform          = Isometry3::Identity();
      const std::string vertex_tag = Factory::instance()->tag(v_it->second);

      if (vertex_tag == "VERTEX_SE3:EULERPERT") {
        transform = dynamic_cast<VertexSE3EulerPert*>(v_it->second)->estimate();
      } else if (vertex_tag == "VERTEX_SE3:ANAXPERT") {
        transform =
          dynamic_cast<VertexSE3AngleAxisPert*>(v_it->second)->estimate();
      } else if (vertex_tag == "VERTEX_SE3:QUAT") {
        transform = dynamic_cast<VertexSE3*>(v_it->second)->estimate();
      } else {
        throw std::runtime_error(
          "ComparatorAction::operator()|invalid vertex type");
      }

      // ia take the corresponding geodesic vertex
      VertexSE3* geodesic_v =
        dynamic_cast<VertexSE3*>(geo_opt_ptr->vertex(v_it->first));

      if (!geodesic_v)
        throw std::runtime_error(
          "ComparatorAction::operator()|dynamic cast failed, exit");

      // ia copy the damn estimate
      geodesic_v->setEstimate(transform);

      ++v_it;
    }

    // ia compute the damn chi2
    geo_opt_ptr->computeActiveErrors();
    chord_opt_ptr->computeActiveErrors();

    const double chi2_chordal     = chord_opt_ptr->activeChi2();
    const double chi2_reprojected = geo_opt_ptr->activeChi2();
    double chi2_chordal_k         = chi2_chordal;
    double chi2_reprojected_k     = chi2_reprojected;

    if (kernel_flag) {
      chi2_chordal_k     = chord_opt_ptr->activeRobustChi2();
      chi2_reprojected_k = geo_opt_ptr->activeRobustChi2();
      if (chord_opt_ptr->activeRobustChi2() == 0)
        return this;
    }

    if (chord_opt_ptr->activeChi2() == 0) {
      return this;
    }

    const double& it_time =
      chord_opt_ptr->batchStatistics()[current_iteration].timeIteration;
    const size_t& nnz =
      chord_opt_ptr->batchStatistics()[current_iteration].choleskyNNZ;

    // ia evaluate linearity of the error function
    // number_t perturbation_norm  = 0.0;
    number_t delta_chi2_chordal = 0.0;
    if (evaluate_error_function_linearity) {
      g2o::OptimizationAlgorithmWithHessian* chord_alg =
        dynamic_cast<g2o::OptimizationAlgorithmWithHessian*>(
          chord_opt_ptr->solver());

      if (!chord_alg) {
        throw std::runtime_error(
          "ComparatorAction::operator()|invalid optimization algorithm");
      }

      // ia norm of delta x
      // perturbation_norm = chord_alg->computePerturbationNorm();
      // ia delta chi2
      delta_chi2_chordal = prev_chi2 - chi2_chordal;
    }

    OptimizationComparisonStats s(current_iteration,
                                  chi2_chordal,
                                  chi2_chordal_k,
                                  it_time,
                                  nnz,
                                  chi2_reprojected,
                                  chi2_reprojected_k,
                                  delta_chi2_chordal,
                                  0.0);

    // ia shit out stuff on the console
    if (verbose_flag) {
      std::cerr << s << std::endl;
    }

    // ia to evaluate delta chi2
    prev_chi2 = chi2_chordal;
    stats_vector.push_back(s);

    return this;
  }
};

int main(int argc, char** argv) {
  OptimizableGraph::initMultiThreading();
  // Command line parsing
  bool initial_guess_spanning;
  bool initial_guess_odometry;
  bool initial_guess_chordal;

  string initial_guess_type("optimum");

  bool list_types_flag;
  bool list_solvers_flag;
  bool list_kernels_flag;

  int max_iterations;
  double kernel_width;

  bool verbose;

  string output_filename;
  string chordal_input_filename;
  string kernel_type;
  string stats_filename;
  string g2ostats_filename;
  string summary_filaname;
  string reference_graph_filename;
  string solver_type;

  bool optimization_has_kernel = false;
  string types_library;

  g2o::CommandArgs arg;
  arg.param("typeslib",
            types_library,
            "",
            "specify a types library which will be loaded");
  arg.param("listTypes", list_types_flag, false, "list the registered types");
  arg.param("listRobustKernels",
            list_kernels_flag,
            false,
            "list the registered robust kernels");
  arg.param(
    "listSolvers", list_solvers_flag, false, "list the available solvers");
  arg.param("v", verbose, false, "verbose output of the optimization process");
  arg.param("i", max_iterations, 10, "perform n iterations");
  arg.param("o", output_filename, "", "output final version of the graph");
  arg.param("guess",
            initial_guess_spanning,
            false,
            "initial guess based on spanning tree");
  arg.param("guessOdometry",
            initial_guess_odometry,
            false,
            "initial guess based on odometry");
  arg.param("guessChordal",
            initial_guess_chordal,
            false,
            "initial guess based on chordal initialization");
  arg.param("robustKernel", kernel_type, "", "use this robust error function");
  arg.param("robustKernelWidth",
            kernel_width,
            -1.,
            "width for the robust Kernel (only if robust_kernel_type)");
  arg.param("solver",
            solver_type,
            "gn_fix6_3_cholmod",
            "specify which solver to use underneat\n\t {gn_var, lm_fix3_2, "
            "gn_fix6_3, lm_fix7_3}");
  arg.param("g2ostats",
            g2ostats_filename,
            "",
            "specify a file for the stats [g2o_stats]");
  arg.param(
    "stats",
    stats_filename,
    "",
    "specify a file for the stats [custom stats containing also reproj chi2]");
  arg.param("summary",
            summary_filaname,
            "",
            "summary of the optimization - for tables :)");
  arg.param("geodesicGraph",
            reference_graph_filename,
            "",
            "reference graph to compare - should be VERTEXSE3:QUAT");
  arg.paramLeftOver("graph-input",
                    chordal_input_filename,
                    "",
                    "graph file which will be processed - 3d pgo built with "
                    "the chordal plugin");
  arg.parseArgs(argc, argv);

  // registering all the types from the libraries
  DlWrapper dlTypesWrapper;
  loadStandardTypes(dlTypesWrapper, argc, argv);
  // register all the solvers
  DlWrapper dlSolverWrapper;
  loadStandardSolver(dlSolverWrapper, argc, argv);

  // ia factory things
  OptimizationAlgorithmFactory* solver_factory =
    OptimizationAlgorithmFactory::instance();
  if (list_solvers_flag) {
    solver_factory->listSolvers(std::cerr);
    return 0;
  }

  if (list_types_flag) {
    Factory::instance()->printRegisteredTypes(std::cerr, true);
    return 0;
  }

  if (list_kernels_flag) {
    std::vector<std::string> available_kernels;
    RobustKernelFactory::instance()->fillKnownKernels(available_kernels);
    std::cerr << "Robust Kernels:" << std::endl;
    for (size_t i = 0; i < available_kernels.size(); ++i) {
      std::cerr << available_kernels[i] << std::endl;
    }

    return 0;
  }

  // ia checking that you are not dumb
  if (reference_graph_filename == "") {
    throw std::runtime_error("no comparison graph, exit");
  }

  if (stats_filename == "") {
    throw std::runtime_error("no comparison stats filename, exit");
  }

  if (chordal_input_filename == "") {
    throw std::runtime_error("no input man, exit");
  }

  // ----------------------------------------------------------------------------------
  // //
  // ----------------------------------------------------------------------------------
  // //
  // -------------------------------- optimizer setup
  // --------------------------------- //
  // ----------------------------------------------------------------------------------
  // //
  // ----------------------------------------------------------------------------------
  // //

  // ia setup chordal optimizer
  g2o::SparseOptimizer optimizer;
  optimizer.setVerbose(false);

  // ia create a solver for the chordal optimizer
  OptimizationAlgorithmProperty solver_property;
  optimizer.setAlgorithm(
    solver_factory->construct(solver_type, solver_property));
  if (!optimizer.solver()) {
    throw std::runtime_error("error while allocating the solver, exit");
  }

  // ia read the damn chordal file
  std::cerr << "opening file: " << chordal_input_filename << std::endl;
  std::ifstream input_stream_chordal(chordal_input_filename.c_str());
  if (!input_stream_chordal)
    throw std::runtime_error("failed to open the selected file, exit");
  optimizer.load(input_stream_chordal);
  const size_t optimizer_num_v = optimizer.vertices().size();
  const size_t optimizer_num_e = optimizer.edges().size();
  std::cerr << "loaded " << optimizer_num_v << " vertices" << std::endl;
  std::cerr << "loaded " << optimizer_num_e << " edges" << std::endl;

  // ia check that the solver is compatible
  std::set<int> vertex_dimension = optimizer.dimensions();
  if (!optimizer.isSolverSuitable(solver_property, vertex_dimension)) {
    throw std::runtime_error(
      "solver is not suitable for optimizing the chordal graph, exit");
  }

  // ia we assume that the gauge is already specified, to avoid that g2o fixes
  // different vertices ia between the two graphs
  if (optimizer.gaugeFreedom())
    throw std::runtime_error(
      "selected chordal graph has no fixed vertices, exit");

  // ia robust kernels, if present
  if (kernel_type.size() > 0) {
    optimization_has_kernel = true;
    std::cerr << "using kernel " << kernel_type << std::endl;
    AbstractRobustKernelCreator* kernel_factory =
      RobustKernelFactory::instance()->creator(kernel_type);
    if (!kernel_factory)
      throw std::runtime_error(
        "unknown kernel type type, call -listKernels man");

    SparseOptimizer::EdgeSet::iterator edge_it   = optimizer.edges().begin();
    SparseOptimizer::EdgeSet::iterator edges_end = optimizer.edges().end();
    while (edge_it != edges_end) {
      SparseOptimizer::Edge* e = dynamic_cast<SparseOptimizer::Edge*>(*edge_it);
      e->setRobustKernel(kernel_factory->construct());
      if (kernel_width > 0)
        e->robustKernel()->setDelta(kernel_width);
      ++edge_it;
    }
  }

  // ia final tweaks
  optimizer.setComputeBatchStatistics(true);

  // ----------------------------------------------------------------------------------
  // //
  // ----------------------------------------------------------------------------------
  // //
  // ---------------------------- reference optimizer setup
  // --------------------------- //
  // ----------------------------------------------------------------------------------
  // //
  // ----------------------------------------------------------------------------------
  // //

  // ia setup geodesic optimizer
  g2o::SparseOptimizer reference_optimizer;
  reference_optimizer.setVerbose(true);

  // ia create a solver for the chordal optimizer
  OptimizationAlgorithmProperty solver_property_reference;
  reference_optimizer.setAlgorithm(
    solver_factory->construct(solver_type, solver_property_reference));
  if (!reference_optimizer.solver()) {
    throw std::runtime_error(
      "error while allocating the solver for the standard graph, exit");
  }

  // ia read the damn geodesic file
  std::cerr << "opening reference file: " << reference_graph_filename
            << std::endl;
  std::ifstream input_stream_reference(reference_graph_filename.c_str());
  if (!input_stream_reference)
    throw std::runtime_error("failed to open the reference file, exit");
  reference_optimizer.load(input_stream_reference);
  const size_t reference_optimizer_num_v =
    reference_optimizer.vertices().size();
  const size_t reference_optimizer_num_e = reference_optimizer.edges().size();
  std::cerr << "loaded " << reference_optimizer_num_v << " vertices"
            << std::endl;
  std::cerr << "loaded " << reference_optimizer_num_e << " edges" << std::endl;

  // ia check that the solver is compatible
  std::set<int> reference_vertex_dimension = reference_optimizer.dimensions();
  if (!reference_optimizer.isSolverSuitable(solver_property,
                                            reference_vertex_dimension)) {
    throw std::runtime_error(
      "solver is not suitable for optimizing the reference graph, exit");
  }

  // ia we assume that the gauge is already specified, to avoid that g2o fixes
  // different vertices ia between the two graphs
  if (reference_optimizer.gaugeFreedom())
    throw std::runtime_error(
      "selected reference graph has no fixed vertices, exit");

  // ia robust kernels, if present
  if (kernel_type.size() > 0) {
    std::cerr << "kernelize the reference graph" << std::endl;
    AbstractRobustKernelCreator* kernel_factory =
      RobustKernelFactory::instance()->creator(kernel_type);
    if (!kernel_factory)
      throw std::runtime_error(
        "unknown kernel type type, call -listKernels man");

    SparseOptimizer::EdgeSet::iterator edge_it =
      reference_optimizer.edges().begin();
    SparseOptimizer::EdgeSet::iterator edges_end =
      reference_optimizer.edges().end();
    while (edge_it != edges_end) {
      SparseOptimizer::Edge* e = dynamic_cast<SparseOptimizer::Edge*>(*edge_it);
      e->setRobustKernel(kernel_factory->construct());
      if (kernel_width > 0)
        e->robustKernel()->setDelta(kernel_width);
      ++edge_it;
    }
  }

  // ia final tweaks
  reference_optimizer.setComputeBatchStatistics(true);

  // ----------------------------------------------------------------------------------
  // //
  // ----------------------------------------------------------------------------------
  // //
  // --------------------------- start the damn computation
  // --------------------------- //
  // ----------------------------------------------------------------------------------
  // //
  // ----------------------------------------------------------------------------------
  // //

  // ia install our custom action
  ComparatorAction* action = new ComparatorAction(
    &reference_optimizer, &optimizer, optimization_has_kernel, verbose);
  optimizer.addPreIterationAction(action);

  // ia initialize optimization
  optimizer.initializeOptimization();
  optimizer.computeActiveErrors();
  const double load_chi2 = optimizer.activeChi2();

  reference_optimizer.initializeOptimization();
  reference_optimizer.computeActiveErrors();
  const double reference_load_chi2 = reference_optimizer.activeChi2();

  std::cerr << "original graph load chi2  : " << FIXED(load_chi2) << std::endl;
  std::cerr << "reference graph load chi2 : " << FIXED(reference_load_chi2)
            << std::endl;

  // ia damn intial guess
  uint8_t initial_guesses = 0;
  if (initial_guess_spanning) {
    std::cerr << "computing initial guess from spanning tree" << std::endl;
    initial_guess_type = "spanning";
    optimizer.computeInitialGuess();
    ++initial_guesses;
  }

  if (initial_guess_odometry) {
    std::cerr << "computing initial guess from odometry" << std::endl;
    initial_guess_type = "odometry";
    EstimatePropagatorCostOdometry chordal_odometry_cost_function(&optimizer);
    EstimatePropagatorCostOdometry geodesic_odometry_cost_function(
      &reference_optimizer);
    optimizer.computeInitialGuess(chordal_odometry_cost_function);
    ++initial_guesses;
  }

  if (initial_guess_chordal) {
    std::cerr << "computing chordal initialization of the graph" << std::endl;
    initial_guess_type = "chordal_initialization";
    ChordalInitializerSE3::initializeGraph(optimizer, verbose);
    ++initial_guesses;
  }

  // ia get the damn initial chi2
  double initial_chi2 = -1.0;
  optimizer.computeActiveErrors();
  initial_chi2 = optimizer.activeChi2();
  std::cerr << "original graph initial chi2: " << FIXED(initial_chi2)
            << std::endl;
  if (optimization_has_kernel)
    std::cerr << "original graph initial chi2 [kernelized]: "
              << FIXED(optimizer.activeRobustChi2()) << std::endl;

  // ua optimize
  int optimization_result = optimizer.optimize(max_iterations);
  if (optimization_result == OptimizationAlgorithm::Fail) {
    std::cerr << "cholesky failed" << std::endl;
  }

  // ia get the damn final chi2
  double final_chi2 = -1.0;
  optimizer.computeActiveErrors();
  final_chi2 = optimizer.activeChi2();
  if (optimization_has_kernel)
    std::cerr << "original graph final chi2 [kernelized]: "
              << FIXED(optimizer.activeRobustChi2()) << std::endl;

  // ia save the output
  if (output_filename != "") {
    cerr << "saving chordal output file : " << output_filename << endl;
    optimizer.save(output_filename.c_str());
    cerr << "done!" << endl;
  }

  // ia save comparative stats
  if (stats_filename != "") {
    std::ofstream comp_stats_stream(stats_filename);
    std::cerr << "writing statistic to file: " << stats_filename << std::endl;

    const int vector_size = action->stats_vector.size();
    if (vector_size < max_iterations) {
      // ia copy the last stats
      const OptimizationComparisonStats& last_s = action->stats_vector.back();
      for (int i = 0; i < max_iterations - vector_size; ++i) {
        action->stats_vector.push_back(
          OptimizationComparisonStats(i + vector_size - 1,
                                      last_s.chi2,
                                      last_s.chi2_k,
                                      last_s.it_time,
                                      last_s.nnz,
                                      last_s.reproj_chi2,
                                      last_s.reproj_chi2_k));
      }
    }

    for (const OptimizationComparisonStats& s : action->stats_vector) {
      comp_stats_stream << s << std::endl;
    }
    std::cerr << "done." << std::endl;
    comp_stats_stream.close();
  }

  // ia shit out a summary useful for latex tables
  if (summary_filaname != "") {
    std::cerr << "writing summary to file: " << summary_filaname << " ... ";
    PropertyMap summary;
    summary.makeProperty<StringProperty>("filename", chordal_input_filename);
    summary.makeProperty<IntProperty>("n_vertices",
                                      optimizer.vertices().size());
    summary.makeProperty<IntProperty>("n_edges", optimizer.edges().size());
    summary.makeProperty<StringProperty>("initial_guess_type",
                                         initial_guess_type);
    summary.makeProperty<IntProperty>("num_iterations", max_iterations - 1);
    summary.makeProperty<StringProperty>("solver_type", solver_type);
    summary.makeProperty<DoubleProperty>("initial_chi", initial_chi2);
    summary.makeProperty<DoubleProperty>("final_chi", final_chi2);

    std::ofstream summary_stream(summary_filaname.c_str());
    summary.writeToCSV(summary_stream);
    summary_stream.close();
    std::cerr << "done." << std::endl;
  }

  // ia shit out the statitics
  if (g2ostats_filename != "") {
    std::cerr << "writing default g2o stats to file: " << g2ostats_filename
              << " ... ";
    ofstream time_stats_stream(g2ostats_filename.c_str());

    BatchStatisticsContainer complete_bsc = optimizer.batchStatistics();

    for (size_t i = 0; i < complete_bsc.size(); i++) {
      const G2OBatchStatistics& s = complete_bsc[i];
      time_stats_stream << "iteration= " << s.iteration << "; ";
      time_stats_stream << "iterationTime= " << s.timeIteration << "; ";
      time_stats_stream << std::endl;
    }
    std::cerr << "done." << std::endl;
    // ia porco dio close the stats
    time_stats_stream.close();
  }

  // ia clean-up
  delete action;
  return 0;
}

Matrix12 _remapInformationMatrix(const Vector6& src_mean_,
                                 const Matrix6& src_omega_,
                                 const double& threshold_) {
  // ia computing sigma_12D
  Matrix6 src_sigma = src_omega_.inverse();
  Isometry3 T_mean  = internal::fromVectorMQT(src_mean_);

  Vector12 remapped_mean  = Vector12::Zero();
  Matrix12 remapped_sigma = Matrix12::Zero();

  SigmaPoint6Vector sigma_points_6D;
  SigmaPoint12Vector sigma_points_12D;

  Vector6 zero_mean = Vector6::Zero();
  if (!sampleUnscented(sigma_points_6D, zero_mean, src_sigma))
    throw std::runtime_error("bad things happened while sampling");

  int k = 1;
  for (int i = 0; i < src_mean_.size(); ++i) {
    int sample_plus_idx  = k++;
    int sample_minus_idx = k++;

    const Vector6& sample_6D_plus  = sigma_points_6D[sample_plus_idx]._sample;
    const Vector6& sample_6D_minus = sigma_points_6D[sample_minus_idx]._sample;

    Isometry3 T_plus  = internal::fromVectorMQT(sample_6D_plus);
    Isometry3 T_minus = internal::fromVectorMQT(sample_6D_minus);

    Vector12 sample_12D_plus  = internal::toFlatten(T_mean * T_plus);
    Vector12 sample_12D_minus = internal::toFlatten(T_mean * T_minus);

    SigmaPoint12 point_12D_plus(sample_12D_plus,
                                sigma_points_6D[sample_plus_idx]._wi,
                                sigma_points_6D[sample_plus_idx]._wp);
    SigmaPoint12 point_12D_minus(sample_12D_minus,
                                 sigma_points_6D[sample_minus_idx]._wi,
                                 sigma_points_6D[sample_minus_idx]._wp);
    sigma_points_12D.push_back(point_12D_plus);
    sigma_points_12D.push_back(point_12D_minus);
  }

  reconstructGaussian(remapped_mean, remapped_sigma, sigma_points_12D);

  // ia reconditioning the new covariance
  Matrix12 conditioned_sigma = _reconditionateSigma(remapped_sigma, threshold_);
  Matrix12 remapped_omega    = conditioned_sigma.inverse();
  return remapped_omega;
}

Matrix12 _reconditionateSigma(const Matrix12& src_sigma_,
                              const double& threshold_) {
  Matrix12 conditioned_sigma = Matrix12::Zero();
  Eigen::JacobiSVD<Matrix12> svd(src_sigma_,
                                 Eigen::ComputeThinU | Eigen::ComputeThinV);
  double conditioned_eigenvalue = 1.0;
  for (int i = 0; i < 12; ++i) {
    if (svd.singularValues()(i, i) < threshold_) {
      conditioned_eigenvalue = svd.singularValues()(i, i) + threshold_;
    } else {
      conditioned_eigenvalue = svd.singularValues()(i, i);
    }
    conditioned_sigma.noalias() += conditioned_eigenvalue *
                                   svd.matrixU().col(i) *
                                   svd.matrixU().col(i).transpose();
    // std::cerr << "conditioned_eigenvalue = " << conditioned_eigenvalue <<
    // std::endl;
  }
  return conditioned_sigma;
}
