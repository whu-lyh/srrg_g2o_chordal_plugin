#include <iostream>
#include <string>
#include <map>

//ia include fancy colors
#include "g2o/stuff/color_macros.h"

//ia include types
#include "g2o/types/slam3d/isometry3d_mappings.h"
#include "g2o/types/slam3d/types_slam3d.h"
#include "types_chordal3d.h"

//ia include g2o core stuff
#include "g2o/stuff/command_args.h"
#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/factory.h"
#include "g2o/core/block_solver.h"
#include "g2o/solvers/cholmod/linear_solver_cholmod.h"
#include "g2o/solvers/csparse/linear_solver_csparse.h"
#include "g2o/core/optimization_algorithm.h"
#include "g2o/core/optimization_algorithm_gauss_newton.h"

//ia dl loader
#include "g2o/apps/g2o_cli/dl_wrapper.h"
#include "g2o/apps/g2o_cli/output_helper.h"
#include "g2o/apps/g2o_cli/g2o_common.h"

const std::string exe_name = "test_multiverse_of_nodes_and_edges|";
#define LOG std::cerr << exe_name

using namespace g2o;

int main(int argc, char **argv) {
  LOG << "registering types...\n";
  // registering all the types from the libraries
  DlWrapper dl_types_wrapper;
  loadStandardTypes(dl_types_wrapper, argc, argv);
  LOG << CL_GREEN("done\n");

  // arguments of the executable
  std::string out_filename, input_filename;
  g2o::CommandArgs arg;
  arg.paramLeftOver("graph-input", input_filename, "", "graph file which will be processed - generated with g2o_simulator3d");
  arg.parseArgs(argc, argv);

  if (input_filename.empty()) {
    throw std::runtime_error("please specify an in file");
  }

  LOG << "loading graph [" << CL_YELLOW(input_filename) << "]" << std::endl;
  SparseOptimizer optimizer;
  optimizer.load(input_filename.c_str());

  const auto& src_vertices = optimizer.vertices();
  const auto& src_edges    = optimizer.edges();

  LOG << "graph has "
      << CL_YELLOW(src_vertices.size()) << " vertices and "
      << CL_YELLOW(src_edges.size()) << " egdes\n";


  g2o::SparseOptimizer g_euler_pert_quaternion_error;
  g2o::SparseOptimizer g_euler_pert_chordal_error;
  g2o::SparseOptimizer g_euler_pert_euler_error;
  g2o::SparseOptimizer g_quaternion_pert_chordal_error;
  g2o::SparseOptimizer g_quaternion_pert_euler_error;

  LOG << "converting vertices...\n";
  for (const auto& v_tuple : src_vertices) {
    VertexSE3* v_quaternion = dynamic_cast<VertexSE3*>(v_tuple.second);

    VertexSE3* v_quaternion_copy_a = new VertexSE3();
    VertexSE3* v_quaternion_copy_b = new VertexSE3();

    VertexSE3EulerPert* v_euler = new VertexSE3EulerPert();
    VertexSE3EulerPert* v_euler_copy_a = new VertexSE3EulerPert();
    VertexSE3EulerPert* v_euler_copy_b = new VertexSE3EulerPert();

    v_euler->setId(v_tuple.first);
    v_euler->setEstimate(v_quaternion->estimate());
    v_euler_copy_a->setId(v_tuple.first);
    v_euler_copy_a->setEstimate(v_quaternion->estimate());
    v_euler_copy_b->setId(v_tuple.first);
    v_euler_copy_b->setEstimate(v_quaternion->estimate());
    v_quaternion_copy_a->setId(v_tuple.first);
    v_quaternion_copy_a->setEstimate(v_quaternion->estimate());
    v_quaternion_copy_b->setId(v_tuple.first);
    v_quaternion_copy_b->setEstimate(v_quaternion->estimate());

    g_euler_pert_quaternion_error.addVertex(v_euler);
    g_euler_pert_chordal_error.addVertex(v_euler_copy_a);
    g_euler_pert_euler_error.addVertex(v_euler_copy_b);
    g_quaternion_pert_chordal_error.addVertex(v_quaternion_copy_a);
    g_quaternion_pert_euler_error.addVertex(v_quaternion_copy_b);
  }
  LOG << CL_GREEN("done\n");


  LOG << "converting edges (ignoring omegas)...\n";
  for (g2o::HyperGraph::Edge* e : src_edges) {
    EdgeSE3* edge_quaternion_quaterion_pert = dynamic_cast<EdgeSE3*>(e);
    const int& v_id_from = edge_quaternion_quaterion_pert->vertices()[0]->id();
    const int& v_id_to   = edge_quaternion_quaterion_pert->vertices()[1]->id();
    const Isometry3& measurement_T =  edge_quaternion_quaterion_pert->measurement();

    //ia quaternion error and euler perturbation
    EdgeSE3QuaternionErrorA* edge_quaternion_euler_pert = new EdgeSE3QuaternionErrorA();
    edge_quaternion_euler_pert->setVertex(0, g_euler_pert_quaternion_error.vertex(v_id_from));
    edge_quaternion_euler_pert->setVertex(1, g_euler_pert_quaternion_error.vertex(v_id_to));
    edge_quaternion_euler_pert->setMeasurement(measurement_T);
    g_euler_pert_quaternion_error.addEdge(edge_quaternion_euler_pert);


    //ia chordal error and euler perturbation
    EdgeSE3ChordalErrorA* edge_chordal_euler_pert = new EdgeSE3ChordalErrorA();
    edge_chordal_euler_pert->setVertex(0, g_euler_pert_chordal_error.vertex(v_id_from));
    edge_chordal_euler_pert->setVertex(1, g_euler_pert_chordal_error.vertex(v_id_to));
    edge_chordal_euler_pert->setMeasurement(measurement_T);
    g_euler_pert_chordal_error.addEdge(edge_chordal_euler_pert);


    //ia chordal error and euler perturbation
    EdgeSE3EulerErrorA* edge_euler_euler_pert = new EdgeSE3EulerErrorA();
    edge_euler_euler_pert->setVertex(0, g_euler_pert_euler_error.vertex(v_id_from));
    edge_euler_euler_pert->setVertex(1, g_euler_pert_euler_error.vertex(v_id_to));
    edge_euler_euler_pert->setMeasurement(measurement_T);
    g_euler_pert_euler_error.addEdge(edge_euler_euler_pert);


    //ia chordal error and quaternion perturbation
    EdgeSE3ChordalErrorB* edge_chordal_quaternion_pert = new EdgeSE3ChordalErrorB();
    edge_chordal_quaternion_pert->setVertex(0, g_quaternion_pert_chordal_error.vertex(v_id_from));
    edge_chordal_quaternion_pert->setVertex(1, g_quaternion_pert_chordal_error.vertex(v_id_to));
    edge_chordal_quaternion_pert->setMeasurement(measurement_T);
    g_quaternion_pert_chordal_error.addEdge(edge_chordal_quaternion_pert);


    //ia chordal error and quaternion perturbation
    EdgeSE3EulerErrorB* edge_euler_quaternion_pert = new EdgeSE3EulerErrorB();
    edge_euler_quaternion_pert->setVertex(0, g_quaternion_pert_euler_error.vertex(v_id_from));
    edge_euler_quaternion_pert->setVertex(1, g_quaternion_pert_euler_error.vertex(v_id_to));
    edge_euler_quaternion_pert->setMeasurement(measurement_T);
    g_quaternion_pert_euler_error.addEdge(edge_euler_quaternion_pert);
  }
  LOG << CL_GREEN("done\n");


  std::printf("%sg_euler_pert_quaternion_error --- [\033[32m%06lu\033[0m] vertices and [\033[32m%06lu\033[0m] edges\n",
              exe_name.c_str(), g_euler_pert_quaternion_error.vertices().size(), g_euler_pert_quaternion_error.edges().size());
  std::printf("%sg_euler_pert_chordal_error ------ [\033[32m%06lu\033[0m] vertices and [\033[32m%06lu\033[0m] edges\n",
              exe_name.c_str(), g_euler_pert_chordal_error.vertices().size(), g_euler_pert_chordal_error.edges().size());
  std::printf("%sg_euler_pert_euler_error -------- [\033[32m%06lu\033[0m] vertices and [\033[32m%06lu\033[0m] edges\n",
              exe_name.c_str(), g_euler_pert_euler_error.vertices().size(), g_euler_pert_euler_error.edges().size());
  std::printf("%sg_quaternion_pert_chordal_error - [\033[32m%06lu\033[0m] vertices and [\033[32m%06lu\033[0m] edges\n",
              exe_name.c_str(), g_quaternion_pert_chordal_error.vertices().size(), g_quaternion_pert_chordal_error.edges().size());
  std::printf("%sg_quaternion_pert_euler_error --- [\033[32m%06lu\033[0m] vertices and [\033[32m%06lu\033[0m] edges\n",
              exe_name.c_str(), g_quaternion_pert_euler_error.vertices().size(), g_quaternion_pert_euler_error.edges().size());

  LOG << "saving graphs...\n";
  g_euler_pert_quaternion_error.save((input_filename+"_eulerPert_quatError").c_str()); //ia TODO explodes
  g_euler_pert_chordal_error.save((input_filename+"_eulerPert_chordError").c_str());
  g_euler_pert_euler_error.save((input_filename+"_eulerPert_eulerError").c_str());
  g_quaternion_pert_chordal_error.save((input_filename+"_quatPert_chordError").c_str()); //ia TODO explodes
  g_quaternion_pert_euler_error.save((input_filename+"_quatPert_eulerError").c_str());
  LOG << CL_GREEN("done\n");

  return 0;
}
