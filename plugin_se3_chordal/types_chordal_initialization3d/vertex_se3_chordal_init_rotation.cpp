#include "vertex_se3_chordal_init_rotation.h"
namespace g2o {

  VertexSE3ChordalInitRotation::VertexSE3ChordalInitRotation() :
        BaseVertex<9, Isometry3>() {
    setToOriginImpl();
    updateCache();
  }

  bool VertexSE3ChordalInitRotation::read(std::istream& is) {
    Vector7 est;
    for (int i=0; i<7; i++)
      is  >> est[i];
    setEstimate(internal::fromVectorQT(est));
    return true;
  }

  bool VertexSE3ChordalInitRotation::write(std::ostream& os) const {
    Vector7 est=internal::toVectorQT(_estimate);
    for (int i=0; i<7; i++)
      os << est[i] << " ";
    return os.good();
  }

} //ia end namespace g2o
