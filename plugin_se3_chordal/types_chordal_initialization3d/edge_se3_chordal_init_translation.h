#pragma once
#include "g2o/core/base_binary_edge.h"
#include "g2o/types/slam3d/g2o_types_slam3d_api.h"
#include "vertex_se3_chordal_init_translation.h"
namespace g2o {

  class G2O_TYPES_SLAM3D_API EdgeSE3ChordalInitTranslation: public BaseBinaryEdge<3, Isometry3, VertexSE3ChordalInitTranslation, VertexSE3ChordalInitTranslation>  {
  public:
    EdgeSE3ChordalInitTranslation();
    virtual ~EdgeSE3ChordalInitTranslation() {}

    //! @brief read/write in a stream
    bool read(std::istream& is) override;
    bool write(std::ostream& os) const override;

    //! @brief computes the error according to the approximation
    //!        of the chordal distance between the two quantities
    void computeError() override;

    //! @brief computes the right jacobians
    void linearizeOplus() override;

    void setMeasurement(const Isometry3& meas) override {
      _measurement = meas;
    }

    bool setMeasurementFromState() override;

    number_t initialEstimatePossible(const OptimizableGraph::VertexSet& /*from*/,
                                     OptimizableGraph::Vertex* /*to*/)  override {
      return 0;
    }

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} /* namespace g2o */

