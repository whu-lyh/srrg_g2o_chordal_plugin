#include "chordal_initializer_se3.h"

namespace g2o {


  void ChordalInitializerSE3::initializeGraph(SparseOptimizer& graph_, const bool& verbose_) {
    std::cerr << "ChordalInitializerSE3::initializeGraph|" << CL_LIGHTBLUE("chordal initialization started") << std::endl;
    //ia gather object tags
//    VertexSE3* v_quat = new VertexSE3();
//    VertexSE3EulerPert* v_euler = new VertexSE3EulerPert();
//    VertexSE3AngleAxisPert* v_aa = new VertexSE3AngleAxisPert();
//    EdgeSE3QuaternionErrorA* e_quat_euler = new EdgeSE3QuaternionErrorA();
//    EdgeSE3* e_quat_quat = new EdgeSE3();
//    EdgeSE3ChordalErrorA* e_chord_euler = new EdgeSE3ChordalErrorA();
//    EdgeSE3ChordalErrorB* e_chord_quat = new EdgeSE3ChordalErrorB();
//
//    const std::string& tag_v_quat = Factory::instance()->tag(v_quat);
//    const std::string& tag_v_euler = Factory::instance()->tag(v_euler);
//    const std::string& tag_v_aa = Factory::instance()->tag(v_aa);
//    const std::string& geo_edge_tag = Factory::instance()->tag(e_quat_quat);
//    const std::string& chord_edge_tag = Factory::instance()->tag(e_chord_euler);
//
//    delete v_quat;   v_quat   = 0;
//    delete v_euler; v_euler = 0;
//    delete e_quat_quat;   e_quat_quat   = 0;
//    delete e_chord_euler; e_chord_euler = 0;

    //ia initialize optimization algorithm
    SparseOptimizer chordal_initializer_graph;
    std::unique_ptr<InitilizationLinearSolverCholmod> linear_solver_0 = g2o::make_unique<InitilizationLinearSolverCholmod>();
    linear_solver_0->setBlockOrdering(true);
    std::unique_ptr<InitilizationBlockSolver> block_solver_0 = g2o::make_unique<InitilizationBlockSolver>(std::move(linear_solver_0));
    g2o::OptimizationAlgorithmGaussNewton* gn_opt_0 = new g2o::OptimizationAlgorithmGaussNewton(std::move(block_solver_0));
    if (!gn_opt_0) throw std::runtime_error("ChordalInitializerSE3::initializeGraph|impossible to create optimization algoritm");
    chordal_initializer_graph.setAlgorithm(gn_opt_0);
    chordal_initializer_graph.setVerbose(verbose_);

    SparseOptimizer translation_initializer_graph;
    std::unique_ptr<InitilizationLinearSolverCholmod> linear_solver_1 = g2o::make_unique<InitilizationLinearSolverCholmod>();
    linear_solver_1->setBlockOrdering(true);
    std::unique_ptr<InitilizationBlockSolver> block_solver_1 = g2o::make_unique<InitilizationBlockSolver>(std::move(linear_solver_1));
    g2o::OptimizationAlgorithmGaussNewton* gn_opt_1 = new g2o::OptimizationAlgorithmGaussNewton(std::move(block_solver_1));
    if (!gn_opt_1) throw std::runtime_error("impossible to create optimization algoritm to perform rotation initialization");
    translation_initializer_graph.setAlgorithm(gn_opt_1);
    translation_initializer_graph.setVerbose(verbose_);


    //ia copy vertex data
    const HyperGraph::VertexIDMap& src_vertices = graph_.vertices();
    const HyperGraph::EdgeSet& src_edges = graph_.edges();

    for (const auto& v_tuple: src_vertices) {
      const std::string& v_tag = Factory::instance()->tag(v_tuple.second);
      OptimizableGraph::Vertex* v = dynamic_cast<OptimizableGraph::Vertex*>(v_tuple.second);
      assert(v && "ChordalInitializerSE3::initializeGraph|corrupted vertex");

      Isometry3 estimate = Isometry3::Identity();
      if (v_tag == "VERTEX_SE3:QUAT")
        estimate = dynamic_cast<VertexSE3*>(v)->estimate();
      else if (v_tag == "VERTEX_SE3:EULERPERT")
        estimate = dynamic_cast<VertexSE3EulerPert*>(v)->estimate();
      else if (v_tag == "VERTEX_SE3:ANAXPERT")
        estimate = dynamic_cast<VertexSE3AngleAxisPert*>(v)->estimate();
      else
        throw std::runtime_error("ChordalInitializerSE3::initializeGraph|unexpected vertex type");

      //ia copy info in the rotation intialization graph
      VertexSE3ChordalInitRotation* v_rot_init = new VertexSE3ChordalInitRotation();
      v_rot_init->setId(v->id());
      v_rot_init->setFixed(v->fixed());
      v_rot_init->setEstimate(estimate);
      chordal_initializer_graph.addVertex(v_rot_init);

      //ia copy info in the translation intialization graph
      VertexSE3ChordalInitTranslation* v_trans_init = new VertexSE3ChordalInitTranslation();
      v_trans_init->setId(v->id());
      v_trans_init->setFixed(v->fixed());
      v_trans_init->setEstimate(estimate);
      translation_initializer_graph.addVertex(v_trans_init);
    }

    //ia copy edge data
    for (HyperGraph::Edge* e: src_edges) {
      const std::string& e_tag = Factory::instance()->tag(e);
      OptimizableGraph::Edge* edge = dynamic_cast<OptimizableGraph::Edge*>(e);
      assert(edge && "ChordalInitializerSE3::initializeGraph|corrupted edge");

      const int& v_id_from = edge->vertices()[0]->id();
      const int& v_id_to = edge->vertices()[1]->id();

      Isometry3 measurement = Isometry3::Identity();
      if (e_tag == "EDGE_SE3:QUAT-A") //ia quaternion error euler pert
        measurement = dynamic_cast<EdgeSE3QuaternionErrorA*>(edge)->measurement();
      else if (e_tag == "EDGE_SE3:QUAT") //ia quaternion error quaternion pert
        measurement = dynamic_cast<EdgeSE3*>(edge)->measurement();
      else if (e_tag == "EDGE_SE3:CHORD-A") //ia chordal error euler pert
        measurement = dynamic_cast<EdgeSE3ChordalErrorA*>(edge)->measurement();
      else if (e_tag == "EDGE_SE3:CHORD-B") //ia chordal error quaternion pert
        measurement = dynamic_cast<EdgeSE3ChordalErrorB*>(edge)->measurement();
      else if (e_tag == "EDGE_SE3:EULER-A") //ia euler error euler pert
        measurement = dynamic_cast<EdgeSE3EulerErrorA*>(edge)->measurement();
      else if (e_tag == "EDGE_SE3:EULER-B") //ia euler error quaternion pert
        measurement = dynamic_cast<EdgeSE3EulerErrorB*>(edge)->measurement();
      else
        throw std::runtime_error("ChordalInitializerSE3::initializeGraph|unexpected edge type");

      //ia copy edge in rot optimizer
      EdgeSE3ChordalInitRotation* e_rot_init = new EdgeSE3ChordalInitRotation();
      e_rot_init->setVertex(0,chordal_initializer_graph.vertex(v_id_from));
      e_rot_init->setVertex(1,chordal_initializer_graph.vertex(v_id_to));
      e_rot_init->setMeasurement(measurement);
      e_rot_init->setInformation(Matrix9::Identity());
      chordal_initializer_graph.addEdge(e_rot_init);

      //ia copy edge in translation optimizer
      EdgeSE3ChordalInitTranslation* e_trans_init = new EdgeSE3ChordalInitTranslation();
      e_trans_init->setVertex(0,translation_initializer_graph.vertex(v_id_from));
      e_trans_init->setVertex(1,translation_initializer_graph.vertex(v_id_to));
      e_trans_init->setMeasurement(measurement);
      e_trans_init->setInformation(Matrix3::Identity());
      translation_initializer_graph.addEdge(e_trans_init);
    }

    if (verbose_) {
      std::cerr << "ChordalInitializerSE3::initializeGraph|converted "
                << CL_YELLOW(chordal_initializer_graph.vertices().size()) << " vertices to [chordal init]" << " and "
                << CL_YELLOW(chordal_initializer_graph.edges().size()) << " edges to [chordal init]"<< std::endl;
      std::cerr << "ChordalInitializerSE3::initializeGraph|converted "
                << CL_YELLOW(translation_initializer_graph.vertices().size()) << " vertices to [translation init]" << " and "
                << CL_YELLOW(translation_initializer_graph.edges().size()) << " edges to [translation init]"<< std::endl;
    }

    if (translation_initializer_graph.gaugeFreedom()||chordal_initializer_graph.gaugeFreedom()) {
      throw std::runtime_error("ChordalInitializerSE3::initializeGraph|graph has no fixed vertices");
    }

    //ia initialize rotations
    chordal_initializer_graph.initializeOptimization();
    chordal_initializer_graph.computeActiveErrors();
//    double chord_initial_chi2 = chordal_initializer_graph.activeChi2();

    if (chordal_initializer_graph.optimize(1) == OptimizationAlgorithm::Fail)
      std::cerr << CL_RED("ChordalInitializerSE3::initializeGraph|Cholesky failed, results may be invalid [chordal init]") << std::endl;

    chordal_initializer_graph.computeActiveErrors();
//    double chord_final_chi2 = chordal_initializer_graph.activeChi2();


    //ia intialize translation from rotation guess
    for (auto v_tuple: chordal_initializer_graph.vertices()) {
      VertexSE3ChordalInitRotation* v = dynamic_cast<VertexSE3ChordalInitRotation*>(v_tuple.second);
      if (!v) throw std::runtime_error("ChordalInitializerSE3::initializeGraph|unexpected vertex type");

      VertexSE3ChordalInitTranslation* v_trans_init = dynamic_cast<VertexSE3ChordalInitTranslation*>(translation_initializer_graph.vertex(v_tuple.first));
      v_trans_init->setEstimate(v->estimate());
    }

    translation_initializer_graph.initializeOptimization();
    translation_initializer_graph.computeActiveErrors();
//    double translation_initial_chi2 = translation_initializer_graph.activeChi2();

    if (translation_initializer_graph.optimize(1) == OptimizationAlgorithm::Fail)
      std::cerr << CL_RED("ChordalInitializerSE3::initializeGraph|Cholesky failed, results may be invalid [translation init]") << std::endl;

    translation_initializer_graph.computeActiveErrors();
//    double translation_final_chi2 = translation_initializer_graph.activeChi2();


    HyperGraph::VertexIDMap& initialized_vertices = translation_initializer_graph.vertices();
    for (auto v_tuple: initialized_vertices) {
      VertexSE3ChordalInitTranslation* v_init = dynamic_cast<VertexSE3ChordalInitTranslation*>(v_tuple.second);
      if (!v_init) throw std::runtime_error("ChordalInitializerSE3::initializeGraph|unexpected vertex type");

      const std::string& v_tag = Factory::instance()->tag(graph_.vertex(v_tuple.first));
      if (v_tag == "VERTEX_SE3:QUAT")
        dynamic_cast<VertexSE3*>(graph_.vertex(v_tuple.first))->setEstimate(v_init->estimate());
      else if (v_tag == "VERTEX_SE3:EULERPERT")
        dynamic_cast<VertexSE3EulerPert*>(graph_.vertex(v_tuple.first))->setEstimate(v_init->estimate());
      else if (v_tag == "VERTEX_SE3:ANAXPERT")
        dynamic_cast<VertexSE3AngleAxisPert*>(graph_.vertex(v_tuple.first))->setEstimate(v_init->estimate());
      else
        throw std::runtime_error("ChordalInitializerSE3::initializeGraph|unexpected vertex type");

    }

    std::cerr << "ChordalInitializerSE3::initializeGraph|" << CL_LIGHTBLUE("chordal initialization finished") << std::endl;
  }

} /* namespace g2o */
