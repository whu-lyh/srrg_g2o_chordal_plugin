#include "vertex_se3_chordal_init_translation.h"

namespace g2o {

  VertexSE3ChordalInitTranslation::VertexSE3ChordalInitTranslation() :
          BaseVertex<3, Isometry3>()
  {
    setToOriginImpl();
    updateCache();
  }

  bool VertexSE3ChordalInitTranslation::read(std::istream& is) {
    Vector7 est;
    for (int i=0; i<7; i++)
      is  >> est[i];
    setEstimate(internal::fromVectorQT(est));
    return true;
  }

  bool VertexSE3ChordalInitTranslation::write(std::ostream& os) const {
    Vector7 est=internal::toVectorQT(_estimate);
    for (int i=0; i<7; i++)
      os << est[i] << " ";
    return os.good();
  }

} /* namespace g2o */
