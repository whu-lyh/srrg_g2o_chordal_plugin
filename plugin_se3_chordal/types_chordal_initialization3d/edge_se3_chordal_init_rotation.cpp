#include "edge_se3_chordal_init_rotation.h"

namespace g2o {

  EdgeSE3ChordalInitRotation::EdgeSE3ChordalInitRotation():
         BaseBinaryEdge<9, Isometry3, VertexSE3ChordalInitRotation, VertexSE3ChordalInitRotation>() {
    _information.setIdentity();
  }

  bool EdgeSE3ChordalInitRotation::read(std::istream& is) {
    Vector7 meas = Vector7::Zero();
    for (uint8_t i = 0; i < 7; ++i) {
      is >> meas[i];
    }

    _measurement = internal::fromVectorQT(meas);

    for (int i = 0; i < _information.rows(); ++i) {
      for (int j = i; j < _information.cols(); ++j) {
        is >> _information(i,j);
        if (i != j)
          _information(j,i) = _information(i,j);
      }
    }

    return true;
  }


  bool EdgeSE3ChordalInitRotation::write(std::ostream& os) const {
    Vector7 meas = internal::toVectorQT(_measurement);
    for (uint8_t i = 0; i < 7; ++i) {
      os << meas[i] << " ";
    }

    for (int r = 0; r < _information.rows(); ++r) {
      for (int c = r; c < _information.cols(); ++c) {
        os << _information(r,c) << " ";
      }
    }
    return os.good();
  }


  void EdgeSE3ChordalInitRotation::computeError() {
    VertexSE3ChordalInitRotation* v_from = static_cast<VertexSE3ChordalInitRotation*>(_vertices[0]);
    VertexSE3ChordalInitRotation* v_to   = static_cast<VertexSE3ChordalInitRotation*>(_vertices[1]);
    if (!v_from || !v_to) throw std::runtime_error("EdgeSE3ChordInit::computeError|unexpected vertex type");
    _error.setZero();

    const Vector12 ti_full = internal::flattenByRow(v_from->estimate());
    const Vector12 tj_full = internal::flattenByRow(v_to->estimate());
    const Matrix12 Mij_full = internal::mprod(_measurement);
    //_error = Mij_full * ti_full - tj_full;

    const Matrix9 Mij_rot = Mij_full.block<9,9>(0,0);
    const Vector9 ti_rot = ti_full.head(9);
    const Vector9 tj_rot = tj_full.head(9);

    _error = Mij_rot * ti_rot - tj_rot;
  }


  void EdgeSE3ChordalInitRotation::linearizeOplus() {
    _jacobianOplusXi.setZero();
    _jacobianOplusXi = internal::mprod(_measurement).block<9,9>(0,0);

    _jacobianOplusXj.setIdentity();
    _jacobianOplusXj = -_jacobianOplusXj;
  }

  bool EdgeSE3ChordalInitRotation::setMeasurementFromState(){
    VertexSE3ChordalInitRotation* from = static_cast<VertexSE3ChordalInitRotation*>(_vertices[0]);
    VertexSE3ChordalInitRotation* to   = static_cast<VertexSE3ChordalInitRotation*>(_vertices[1]);
    Isometry3 delta = from->estimate().inverse() * to->estimate();
    setMeasurement(delta);
    return true;
  }


} /* namespace g2o */
